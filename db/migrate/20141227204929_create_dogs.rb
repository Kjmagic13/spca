class CreateDogs < ActiveRecord::Migration
  def change
    create_table :dogs do |t|
      t.string :name
      t.date :birthday_at
      t.text :description

      t.timestamps
    end
  end
end
