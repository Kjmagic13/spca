module ApplicationHelper

	def nav_link_to(text, href, opts = {})
		active_class = (current_page?(href) ? 'active' : nil)
		content_tag :li, link_to(text, href, opts), class: active_class
	end

end
