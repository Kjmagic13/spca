module Animals
    extend ActiveSupport::Concern

    def age
        now = Time.now.utc.to_date
        now.year - birthday_at.year - (birthday_at.to_date.change(:year => now.year) > now ? 1 : 0)
    end

end